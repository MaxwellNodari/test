﻿using Newtonsoft.Json;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        string directory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        string jsonFilePath = Path.Combine(directory, "Times.json");

        try
        {
            if (File.Exists(jsonFilePath))
            {
                string file = File.ReadAllText(jsonFilePath);

            var times = JsonConvert.DeserializeObject<Times>(file);
            //Quantas partidas o time B ganha do TimeA
            List<int> result = new();
            foreach (var gols in times.TimeB)
            {
                result.Add(times.TimeA.Where(x => x <= gols).Count());
            }
            Console.WriteLine(JsonConvert.SerializeObject(result));
        }
        catch (Exception)
        {

            throw;
        }

    }
}

public class Times
{
    public List<int> TimeA { get; set; }
    public List<int> TimeB { get; set; }
}