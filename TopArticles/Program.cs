﻿using Newtonsoft.Json;
using RestSharp;

internal class Program 
{
    private static void Main(string[] args)
    {

        string apiUrl = "https://ff9920b4-766e-4568-bf72-66331605dc30.mock.pstmn.io/teste/api/articles";
        var articles = new Articles();
        List<TopArticles> topFive = new();
        List<Datum> topArticles = new();
        do
        {
            try
            {
                long page = 1;
                articles = GetArticles(apiUrl, page);

                if (articles?.Data?.Count > 0)
                {
                    var filtered = articles.Data.Where(x => (string.IsNullOrEmpty(x.Title) == false || string.IsNullOrEmpty(x.StoryTitle)) && x.NumComments.GetValueOrDefault(0) != 0);
                    var top = filtered.Filter(5);

                    topArticles.AddRange(top);
                    page++;
                }
                   
            }
            catch (Exception)
            {
                //Log falha
            }


        } while (articles.Page <= articles.TotalPages);


        Console.WriteLine(JsonConvert.SerializeObject(topArticles.Filter(5).BuildResult()));
    }

    private static Articles GetArticles(string apiUrl, long page)
    {
        var client = new RestClient();
        var request = new RestRequest(apiUrl, Method.Get);

        request.AddParameter("page", page);

        var response = client.Execute(request);

        if (response.IsSuccessful)
        {
            var articles = JsonConvert.DeserializeObject<Articles>(response.Content);
            return articles;
        }
        return new() { Data = new() };
    }
}


public partial class TopArticles
{
    public string TopArticlesTitle { get; set; }
}

public partial class Articles
{
    [JsonProperty("page", NullValueHandling = NullValueHandling.Ignore)]
    public long? Page { get; set; }

    [JsonProperty("per_page", NullValueHandling = NullValueHandling.Ignore)]
    public long? PerPage { get; set; }

    [JsonProperty("total", NullValueHandling = NullValueHandling.Ignore)]
    public long? Total { get; set; }

    [JsonProperty("total_pages", NullValueHandling = NullValueHandling.Ignore)]
    public long? TotalPages { get; set; }

    [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
    public List<Datum> Data { get; set; }
}

public partial class Datum
{
    [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
    public string Title { get; set; }

    [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
    public Uri Url { get; set; }

    [JsonProperty("author", NullValueHandling = NullValueHandling.Ignore)]
    public string Author { get; set; }

    [JsonProperty("num_comments")]
    public long? NumComments { get; set; }

    [JsonProperty("story_id")]
    public string StoryId { get; set; }

    [JsonProperty("story_title")]
    public string StoryTitle { get; set; }

    [JsonProperty("story_url")]
    public Uri StoryUrl { get; set; }

    [JsonProperty("parent_id")]
    public string ParentId { get; set; }

    [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
    public DateTimeOffset? CreatedAt { get; set; }
}