internal static class Core
{

    public static IEnumerable<Datum> Filter(this IEnumerable<Datum> filtered, int take)
    {
        return filtered.OrderByDescending(comment => comment.NumComments)
                                .ThenByDescending(tittle => string.IsNullOrEmpty(tittle.Title) ? tittle.StoryTitle : tittle.Title)
                                .Take(take).AsQueryable();
    }

    public static IEnumerable<TopArticles> BuildResult(this IEnumerable<Datum> top)
    {
        return top.Select(a => new TopArticles()
        {
            TopArticlesTitle = string.IsNullOrEmpty(a.Title) ? a.StoryTitle : a.Title + " - Comments: " + a.NumComments,
        });
    }
}